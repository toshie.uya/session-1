"use strict";

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.connect('mongodb://localhost/sample', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    user: "sampleUser",
    pass: "secret"
});

var port = 3000;
var app = (0, _express2.default)();

var dataSchema = new _mongoose2.default.Schema({
    title: String,
    text: String,
    timestamp: Date
});

var recordSchema = new _mongoose2.default.Schema({
    action: String,
    payload: [dataSchema],
    timestamp: Date
});
var dataModel = _mongoose2.default.model("Data", dataSchema);
var recordModel = _mongoose2.default.model("Record", recordSchema);

app.use(_express2.default.json());
app.get("/", function (_, res) {
    res.json({
        message: "Hello World"
    });
});

app.post("/create", async function (req, res) {
    var data = new dataModel({
        title: req.body.title,
        text: req.body.text,
        timestamp: new Date().toISOString()
    });

    var record = new recordModel({
        action: "Create",
        payload: [data],
        timestamp: new Date().toISOString()
    });

    try {
        await data.save();
        await record.save();
    } catch (e) {
        res.json({
            error: e.message
        });
    }

    res.json({
        message: "payload added"
    });
});

app.get("/data", async function (_, res) {
    try {
        var data = await dataModel.find({}, "title text").exec();
        var record = new recordModel({
            action: "Fetch",
            payload: data,
            timestamp: new Date().toISOString()
        });
        await record.save();

        res.json({
            data: data
        });
    } catch (e) {
        res.json({
            error: e.message
        });
    }
});

app.delete("/:id", async function (req, res) {
    try {
        var data = await dataModel.find({ _id: req.params.id }, "title text timestamp").exec();
        var record = new recordModel({
            action: "Deleted",
            payload: data,
            timestamp: new Date().toISOString()
        });
        await record.save();
        await dataModel.deleteOne({ _id: req.params.id }).exec();
        res.json({
            message: req.params.id + " is deleted"
        });
    } catch (e) {
        res.json({
            error: e.message
        });
    }
});

_mongoose2.default.connection.on('error', console.error.bind(console, 'connection error:'));
_mongoose2.default.connection.on("open", function () {
    app.listen(port, function () {
        console.log("Application running on port " + port);
    });
});