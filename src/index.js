import express from "express";
import mongoose from "mongoose";

mongoose.connect('mongodb://localhost/sample', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    user: "sampleUser",
    pass: "secret"
});

const port = 3000;
const app = express();

const dataSchema = new mongoose.Schema({
    title: String,
    text: String,
    timestamp: Date,
})

const recordSchema = new mongoose.Schema({
    action: String,
    payload: [dataSchema],
    timestamp: Date,
})
const dataModel = mongoose.model("Data", dataSchema)
const recordModel = mongoose.model("Record", recordSchema)

app.use(express.json())
app.get("/", (_, res) => {
    res.json({
        message: "Hello World"
    })
})

app.post("/create", async (req, res) => {
    const data = new dataModel({
        title: req.body.title,
        text: req.body.text,
        timestamp: new Date().toISOString(),
    })

    const record = new recordModel({
        action: "Create",
        payload: [data],
        timestamp: new Date().toISOString(),
    })

    try {
        await data.save()
        await record.save()
    } catch (e) {
        res.json({
            error: e.message
        })
    }

    res.json({
        message: "payload added"
    })
})

app.get("/data", async (_, res) => {
    try {
        const data = await dataModel.find({}, "title text").exec()
        const record = new recordModel({
            action: "Fetch",
            payload: data,
            timestamp: new Date().toISOString(),
        })
        await record.save()

        res.json({
            data,
        })
    } catch (e) {
        res.json({
            error: e.message
        })
    }
})

app.delete("/:id", async (req, res) => {
    try {
        const data = await dataModel.find({_id: req.params.id}, "title text timestamp").exec()
        const record = new recordModel({
            action: "Deleted",
            payload: data,
            timestamp: new Date().toISOString(),
        })
        await record.save()
        await dataModel.deleteOne({_id: req.params.id}).exec()
        res.json({
            message: `${req.params.id} is deleted`
        })
    } catch (e) {
        res.json({
            error: e.message
        })
    }
})

mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.on("open", () => {
    app.listen(port, () => {
        console.log(`Application running on port ${port}`);
    })
})
